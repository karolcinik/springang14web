'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', function($scope) {
    $scope.phone_name = 'Some phone name';
    $scope.count = 0;
    $scope.modu = 0;
    $scope.username = "none";

    $scope.calculateModule = function() {
        $scope.modu = $scope.count%2;
    }

});